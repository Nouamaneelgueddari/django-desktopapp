# Generated by Django 2.1.1 on 2018-10-07 13:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Absence', '0002_auto_20181005_1923'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='students',
            name='Semestre',
        ),
        migrations.AddField(
            model_name='students',
            name='Adresse',
            field=models.CharField(default='null', max_length=250),
        ),
    ]
