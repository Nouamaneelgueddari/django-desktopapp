from django.db import models

# Creaste your models here.
class Students(models.Model):
	dict ={
	       'Nom':'',
		   'Prenom':'',
		   'Sexe':'',
		   'Adresse':'',
		   'Telephone':'',
		   'Email':'',
		   'filliere':'',
		   'Semestre':''
	}
	Nom = models.CharField(max_length = 250)
	Prenom =  models.CharField(max_length = 250)
	Sexe=models.CharField(max_length = 250)
	Adresse= models.CharField(max_length = 250, default='null')
	Telephone= models.CharField(null = True , max_length = 250)
	Email= models.EmailField(blank = True)	
	filliere = models.CharField(max_length = 250 ,default='null')
	Semestre = models.CharField(max_length = 250 ,default='null')
	def __str__(self):
		return self.Nom+'-'+self.Prenom +'('+self.filliere+')' 
class salle(models.Model):
		numero = models.IntegerField()
		def __str__(self):
			return  'Salle :'+str(self.numero)
class Horaire(models.Model):
	    horaire = models.CharField(max_length=250)
	    def __str__(self):
	    	return  str(self.horaire)
class Jour(models.Model):
		nom_Jour = models.CharField(max_length = 250)
		def __str__(self):
	        	return str(self.nom_Jour)
class Crenau(models.Model):
	    horaire =models.ForeignKey(Horaire,on_delete=models.CASCADE ,default=None, null=True)
	    salle = models.ForeignKey(salle,on_delete=models.CASCADE,default=None, null=True)
	    Jour= models.ForeignKey(Jour,on_delete=models.CASCADE,default=None, null=True)
	    Matiere = models.CharField(max_length = 250 , default='null')
	    Professeur = models.CharField(max_length = 250,default='null')
	    def __str__(self):
	        	return str(self.horaire)+'-'+str(self.Jour)
class Admin(models.Model):
		email = models.CharField(max_length=250)
		password = models.CharField(max_length=250)
		def __str__(self):
				return str(self.email)
		
		
