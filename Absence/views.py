from django.shortcuts import render
from django.http  import HttpResponse
from django.template import loader
from .models import *
from Absence.forms import *
from django.views.generic import TemplateView
from django.http import JsonResponse
from django.core import serializers
from django.http.response import HttpResponseRedirect
from django.contrib import messages 
from django.views.generic import View
from django.template.loader import get_template


def Front_page(resquest):
    template = loader.get_template('Login.html')
    context = {
    }
    return HttpResponse(template.render(context,resquest))
def Acceuil(resquest):
    email = str(resquest.POST.get('email', None))
    password = str(resquest.POST.get('password',None))
    admins = Admin.objects.all()
    for admin in admins:
        if admin.email == email and admin.password == password:
            all_students = Students.objects.all()
            template = loader.get_template('Front_page.html')
            context = {
            'all_students' :all_students,  
            }
            return HttpResponse(template.render(context,resquest))
        else :
            template = loader.get_template('Login.html')
            context = {
            }
            return HttpResponse(template.render(context,resquest))
def Charts(resquest):
    all_students = Students.objects.all()
    template = loader.get_template('Charts.html')
    context ={
    'all_students' :all_students,  
    }
    return HttpResponse(template.render(context,resquest))

def Tables(resquest):
    all_students = Students.objects.all()
    template = loader.get_template('tables.html')
    context ={
    'all_students' :all_students,  
    }
    return HttpResponse(template.render(context,resquest))    

def Register(resquest):
    template = loader.get_template('register.html')
    context = {
    }
    return HttpResponse(template.render(context,resquest))
def  Emploi (resquest):
    all_salles = salle.objects.all()
    all_horaires = Horaire.objects.all()
    Jours = Jour.objects.all()
    template = loader.get_template('Emploi.html')
    context ={'all_salles': all_salles,'all_horaires':all_horaires,'Jours' : Jours,}
    return render(resquest,'Emploi.html',context)
def Check_salle(resquest):
        jour = str(resquest.GET.get('Jour', None))
        Horaire = str(resquest.GET.get('Horaire', None))
        Salle_choisie = str(resquest.GET.get('Salle_choisie',None))
        all_salles = salle.objects.all()
        all_crenau = Crenau.objects.all()
        for Crenaus in all_crenau :
         if(str(Crenaus.Jour.id)==jour and str(Crenaus.horaire.id)==Horaire):
           all_salles = all_salles.exclude(numero=Crenaus.salle.numero)  
        data ={
            'is_not_taken':all_salles.filter(id=Salle_choisie).exists()
        }          
        return JsonResponse(data)
def submit_to_database(request):
    template = loader.get_template('template2.html')
    if request.method == 'POST':
        Professeur = str(request.POST.get('Prof',False))
        Matiere = str(request.POST.get('matière',False))
        jour = request.POST.get('jour',False)
        Salle = request.POST.get('salle',False)
        horaire = request.POST.get('horaire',False)
        form=NameForm({'Matiere':Matiere , 'Professeur' :Professeur ,'salle':Salle ,'horaire' :horaire ,'Jour' : jour })
        if form.is_valid():       
           form.save()  
    jours = Jour.objects.all()
    Horaires = Horaire.objects.all()
    all_salles = salle.objects.all()
    Crenaus = Crenau.objects.all()
    context = {
    'Jours' : jours,
    'Horaires': Horaires,
    'Crenaus': Crenaus,
    'all_horaires':Horaires,
    'all_salles': all_salles,
    }  
    return HttpResponse(template.render(context,request))
def Emploi_template(request):
    template = loader.get_template('template2.html')
    jours = Jour.objects.all()
    Horaires = Horaire.objects.all()
    Crenaus = Crenau.objects.all()
    all_salles = salle.objects.all()
    for creno in Crenaus:
        print(creno)
    context = {
    'Jours' : jours,
    'Horaires': Horaires,
    'Crenaus': Crenaus,
    'all_horaires':Horaires,
    'all_salles': all_salles,
    }
    return HttpResponse(template.render(context,request)) 
