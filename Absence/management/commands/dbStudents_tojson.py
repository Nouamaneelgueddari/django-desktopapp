from Absence.models import Students
from  django.core.management.base import BaseCommand
from django.core.serializers import serialize
def dbStudents_tojson():
	serialize('json',Students.objects.all())

	
class Command(BaseCommand):
    def handle(self, **options):
        dbStudents_tojson()