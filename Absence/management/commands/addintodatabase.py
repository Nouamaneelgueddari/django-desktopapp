from  django.core.management.base import BaseCommand
import xlrd
import os
from Absence.models import Students
def addintodatabase():
        THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
        my_file = os.path.join(THIS_FOLDER,'C:/Users/HP/Bachlor_App/Absence/static/files/File.xlsx')
        workbook = xlrd.open_workbook(my_file)
        first_row = 1
        worksheet = workbook.sheet_by_index(0)
        nb_rows = worksheet.nrows
        for row in range(first_row, nb_rows ):
                row_value_list = [cell.value for cell in worksheet.row(row)]
                if (type(Students) == dict ):
                    da = zip(Students.dict.keys(),row_value_list)
                    data = dict([(i[0],Students[i[0]](i[1])) for i in da ])
                else:
                    data = dict(zip(Students.dict, row_value_list) )
                    obj = Students(**data)
                    obj.save()
class Command(BaseCommand):
    def handle(self, **options):
        addintodatabase()